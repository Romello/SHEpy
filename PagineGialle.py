# importo la libreria csv che mi permette di fare il parsing di file csv
import csv

# definisco una funzione per portare in un dizionario il contenuto del file
def csv2dictionary(csv_file_name):
    '''
    Una funzione che prende in input una stringa (il nome del file csv) e
    ritorna un dizionario in cui ad ogni chiave è associata una colonna
    del file csv
    '''

    # Creo un dizionario vuoto, che poi andrò a riempire col contenuto del file
    dizionario = {}

    # Apro il file in input con la funzione open(), in modalità lettura ('r')
    # 'r' sta per 'read'
    csv_file = open(csv_file_name, 'r')

    # Creo un oggetto iterabile tramite la funzione reader della classe csv
    csv_reader = csv.reader(csv_file, delimiter=',')

    # Faccio un ciclo sull'oggetto csv_reader riempiendo il dizionario
    for row in csv_reader:
        # Uso come chiave il nome del personaggio
        personaggio = row[0]
        # E come valore associato alla chiave un secondo dizionario
        # che contiene Indirizzo e Telefono del personaggio
        contatti = {"Indirizzo":row[1],"Telefono":row[2]}
        # Alla fine riempio il dizionario con chiavi e valori
        dizionario[personaggio]=contatti

    # Dico alla funzione cosa restituire in output
    return dizionario

# definisco una funzione per trovare un personaggio in base al cognome
def trova_per_cognome(cognome_da_cercare, nome_personaggio):
    '''
    Una funzione che prende in input un cognome da cercare e il nome completo
    di un personaggio e ritorna il valore booleano True se il cognome del
    personaggio corrisponde o False se non corrisponde
    '''

    # Divido il nome del personaggio in nome e cognome grazie alla funzione
    # split, che vuole come input il carattere da usare come riferimento
    # per dividere: nel nostro caso lo spazio tra nome e cognome
    cognome = nome_personaggio.split(' ')[1]

    # Controllo con un if-else se il cognome corrisponde
    if cognome == cognome_da_cercare:
        # Se sì, ritorno True
        return True
    else:
        # Se no, ritorno False
        return False
